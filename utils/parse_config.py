

def parse_model_config(path):
    """Parses the yolo-v3 layer configuration file and returns module definitions"""
    file = open(path, 'r')
    lines = file.read().split('\n')#相当于readlines
    #删除掉注释的字符串
    lines = [x for x in lines if x and not x.startswith('#')]#用于检测文件是否以某一个前缀开始, if x判断x是否为空
    #x.rstrip()删除掉最后的空格，x.lstrip()删掉开头的空格
    lines = [x.rstrip().lstrip() for x in lines] # get rid of fringe whitespaces
    module_defs = []
    #对每一个卷积层创建一个dict, 将参数添加进dict
    for line in lines:
        if line.startswith('['): # This marks the start of a new block, 带中括号的是每一层的名称
            module_defs.append({})
            module_defs[-1]['type'] = line[1:-1].rstrip()#给最后一个字典添加一个'type' index，将[]内的内容添加进dict，用rstrip去掉最后的空格
            if module_defs[-1]['type'] == 'convolutional':
                module_defs[-1]['batch_normalize'] = 0#如果为convolutional那么再添加一个index, 默认batch_size为0
        else:
            key, value = line.split("=")
            value = value.strip()#strip()删除开头或结尾的空格
            module_defs[-1][key.rstrip()] = value.strip()#在最后一个字典上添加key和value

    return module_defs

def parse_data_config(path):
    """Parses the data configuration file"""
    # coco.data中存放class的个数，train的地址，valid的地址，class的名称，backup的存放地址
    options = dict()
    options['gpus'] = '0,1,2,3'# GPU的个数
    options['num_workers'] = '10'# 进程数
    with open(path, 'r') as fp:
        lines = fp.readlines()
    for line in lines:
        line = line.strip()
        if line == '' or line.startswith('#'):
            continue
        key, value = line.split('=')
        options[key.strip()] = value.strip()
    return options
