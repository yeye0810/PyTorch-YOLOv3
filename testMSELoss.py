import torch
import torch.nn as nn
loss = nn.MSELoss()
input = torch.randn(3, 5, requires_grad=True)
print(input)
target = torch.randn(3, 5)
output = loss(input, target)
output.backward()
